function Usuario() {
	
	var id = null;
	
	/*
	 * dados de acesso
	 */
	var email;
 	var senha;
	
	//transient
	var emailConfirmacao;
	
 	var nome;
 	
	var telefones = [];
 	var emails = [];
 	var enderecos = [];
 	
 	//endereco
  	var cep;
 	var estado;
 	var municipio;
 	var bairro;
 	var logradouro;
 	var numero;
 	var complemento;
 
 	//dados sps
 	var latitude;
 	var longitude;
 	var altitude;
 	var accuracy;
 	var altitudeAccuracy;
 	var heading;
 	var speed;
 	var enderecoValidado;
 	var latLongValidado;
 	
  	
 	this.setNumero = function(_numero) {
 		this.numero = _numero;
 	}
 	
 	this.getNumero = function() {
 		return this.numero;
 	}
 	
 	this.setComplemento = function(_complemento) {
 		this.complemento = _complemento;
 	}
 	
 	this.getComplemento = function() {
 		return this.complemento;
 	}
 	
 	this.setCep = function(_cep) {
 		this.cep = _cep;
 	}
 	
 	this.getCep = function() {
 		return this.cep;
 	}
 	
 	this.setEstado = function(_estado) {
 		this.estado = _estado;
 	}
 	
 	this.getEstado = function() {
 		return this.estado;
 	}
 	
 	this.setMunicipio = function(_municipio) {
 		this.municipio = _municipio;
 	}
	
 	this.getMunicipio = function() {
 		return this.municipio;
 	}
 	
 	this.setBairro = function(_bairro) {
 		this.bairro = _bairro;
 	}
 	
 	this.getBairro = function() {
 		return this.bairro;
 	}
 	
 	this.setLogradouro = function(_logradouro) {
 		this.logradouro = _logradouro;
 	}
 	
 	this.getLogradouro = function() {
 		return this.logradouro;
 	}
	this.setId = function(_id) {
		this.id = _id;
	}
	
	this.getId = function() {
		return this.id;
	}
	
	this.setSenha= function(_senha) {
		this.senha = _senha;
	}
	
	this.getSenha = function() {
		return this.senha;
	}

	this.setNome = function(_nome) {
		this.nome = _nome;
	}
	
	this.getNome = function() {
		return this.nome;
	}
	
	this.setEmail = function(_email) {
		this.email = _email;
	}
	
	this.getEmail = function() {
		return this.email;
	}
 	
	this.setEmailConfirmacao = function(_emailConfirmacao) {
		this.emailConfirmacao = _emailConfirmacao;
	}
	
	this.getEmailConfirmacao = function() {
		return this.emailConfirmacao;
	}
	
	this.getTelefones = function() {
		return telefones;
	}
	
	this.getEmails = function() {
		return emails;
	}
	
	this.getEnderecos = function() {
		return enderecos;
	}
}