
function BusinessDelegateEnderecoAtendimentoPrestador() {

	
	var enderecoAtendimentoPrestador = new EnderecoAtendimentoPrestador();
	var daoEnderecoAtendimentoPrestador = new DAOEnderecoAtendimentoPrestado();
	
	this.getEnderecoAtendimentoPrestador = function() {
		return enderecoAtendimentoPrestador;
	}
	this.iniciar = function() {
		
 		if (AppInfo.dropAllTableAoIniciar) {
 			daoEnderecoAtendimentoPrestador.dropTable();
 		}
 		
	    db.database.transaction(this.criarTabela, function(err) {
	    	console.log("Error processing SQL: " + err.code);
	    }, function() {
	    	console.log("success create table " + enderecoAtendimentoPrestadorInfo.NOME_TABELA);
	    });	
	}
	
	this.criarTabela = function(tx) {
		daoEnderecoAtendimentoPrestador.createTable(tx);
	}
	
	this.salvar = function() {
		daoEnderecoAtendimentoPrestador.salvar(enderecoAtendimentoPrestador);
		return true;
	}
	
	this.excluirAll = function() {
		daoEnderecoAtendimentoPrestador.deletarAll(enderecoAtendimentoPrestador);
	}
}