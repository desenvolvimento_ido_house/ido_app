function BusinessDelegatePrestador() {
	
	var businessServicoPrestador = new BusinessDelegateServicoPrestador();
	var daoPrestador = new DAOPrestador();
	var daoUsuario = new DAOUsuario();
	var daoEmailUsuario = new DAOEmailUsuario();
	var daoTelefoneUsuario = new DAOTelefoneUsuario();
	var daoServicoPrestador = new DAOServicoPrestador();
	var daoEnderecoUsuario = new DAOEnderecoUsuario();
	var daoEnderecoAtendimentoPrestador = new DAOEnderecoAtendimentoPrestado();
	var daoEnderecoAtendimentoBairroPrestador = new DAOEnderecoAtendimentoBairroPrestador();
	
	var prestador = new Prestador();
	
 	this.getPrestador = function() {
		return prestador;
	}
 	
 	this.getBusinessServicoPrestador = function() {
 		return businessServicoPrestador;
 	}
	
 	this.iniciar = function() {
 		
 		businessServicoPrestador.iniciar();
 		
 		if (AppInfo.dropAllTableAoIniciar) {
 			daoPrestador.dropTable();
 		}
 		
	    db.database.transaction(this.criarTabela, function(err) {
	    	console.log("Error processing SQL: " + err.code);
	    }, function() {
	    	console.log("success create table " + prestadorInfo.NOME_TABELA);
	    });	
	}
 	
	this.salvar = function() {
		
		if (prestador.getCnpjCpf() == null || prestador.getCnpjCpf() == '' ) {
			if (prestador.getTipoPessoa() == beanPrestadorInfo.PJ) {
				alert("O CNPJ é de preenchimento obrigatório");
			} else {
				alert("O CPF é de preenchiemnto obrigatório.");
			}
			return false;
		}
		
		if(prestador.getTipoPessoa() == beanPrestadorInfo.PF){
			if(prestador.getDataNascimento() == null || prestador.getDataNascimento() == ''){
				alert("Data de nascimento é de preenchimento obrigatório");
				return false;
			}
		}
		
		if (prestador.getId() != null) {
			daoPrestador.update(prestador);
		} else {
			daoPrestador.salvar(prestador);			
		}
		return true;
	}
	
	this.excluir = function() {
		daoPrestador.deletar(prestador);
 	}
	
	this.criarTabela = function(tx) {
		daoPrestador.createTable(tx);
	}
	
	this.listarAll = function() {
		daoPrestador.listarAll();	
	}
	
	this.getMemoryRows = function() {
		return daoPrestador.getMemoryRows();
	}
	
	this.buscar = function() {
 		daoUsuario.buscar(prestador.getUsuario());
		daoEmailUsuario.buscar(prestador.getUsuario());
		daoTelefoneUsuario.buscar(prestador.getUsuario());
		daoEnderecoUsuario.buscar(prestador.getUsuario());
 		daoServicoPrestador.buscar(prestador);
 		daoEnderecoAtendimentoPrestador.buscar(prestador);
 		daoPrestador.buscar(prestador);
 	}
}