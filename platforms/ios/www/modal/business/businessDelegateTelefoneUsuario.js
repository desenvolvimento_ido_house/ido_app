function BusinessDelegateTelefoneUsuario() {
	
	var telefoneUsuario = new TelefoneUsuario();
	var daoTelefoneUsuario = new DAOTelefoneUsuario();
	
	this.getTelefoneUsuario = function() {
		return telefoneUsuario;
	}
	
	this.iniciar = function() {
		
 		if (AppInfo.dropAllTableAoIniciar) {
 			daoTelefoneUsuario.dropTable();
 		}
 		
	    db.database.transaction(this.criarTabela, function(err) {
	    	console.log("Error processing SQL: " + err.code);
	    }, function() {
	    	console.log("success create table " + telefoneUsuarioInfo.NOME_TABELA);
	    });	
	}
	
	this.salvar = function() {  
 		if (telefoneUsuario.getId() != null) {
			daoTelefoneUsuario.update(telefoneUsuario);
		} else {
			daoTelefoneUsuario.salvar(telefoneUsuario);			
		}
 		return true;
	}
	
	this.excluir = function() {
		daoTelefoneUsuario.deletar(telefoneUsuario);
	}
	
	this.excluirAll = function() {
		daoTelefoneUsuario.deletarAll(telefoneUsuario);
	}
	
	this.criarTabela = function(tx) {
		daoTelefoneUsuario.createTable(tx);
	}
	
}