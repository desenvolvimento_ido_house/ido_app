function BusinessDelegateUsuario() {
	
	var usuario = new Usuario();
	var daoUsuario = new DAOUsuario();
 	
	this.getUsuario = function() {
		return usuario;
	}
	
	this.getDaoUsuario = function(){
		return daoUsuario;
	}
	
 	this.iniciar = function() {
 		
 		if (AppInfo.dropAllTableAoIniciar) {
 			daoUsuario.dropTable();
 		}
 		
	    db.database.transaction(this.criarTabela, function(err) {
	    	console.log("Error processing SQL: " + err.code);
	    }, function() {
	    	console.log("success create table " + usuarioInfo.NOME_TABELA);
	    });	
	}
 	
	this.salvar = function() { 
		
		if (usuario.getNome() == null || usuario.getNome() == '') {
			alert("O nome do usuário é de preenchimento obrigatório");
			return false;
		}
		
		if (usuario.getEmail() == null || usuario.getEmail() == '') {
			alert("O e-mail principal do usuário é de preenchiemnto obrigatório");
			return false;
		}
		
		if (usuario.getSenha() == null || usuario.getSenha() == '') {
			alert('É necessário definir uma senha para o usuário.');
			return false;
		}
		
		if (usuario.getId() != null) {
			daoUsuario.update(usuario);
		} else {
			if (usuario.getEmail() != usuario.getEmailConfirmacao() ) {
				alert("A confirmação de e-mail não confere com seu e-mail principal.");
				return;
			}
			daoUsuario.salvar(usuario);			
		}
		return true;
	}
	
	this.listarAll = function() {
		daoUsuario.listarAll();
	}
	
	this.excluir = function() {
		daoUsuario.deletar(usuario);
	}
	
	this.criarTabela = function(tx) {
		daoUsuario.createTable(tx);
	}
	
	this.getMemoryRows = function() {
		return daoUsuario.getMemoryRows();
	}
	
}