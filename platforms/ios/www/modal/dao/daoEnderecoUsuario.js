var daoEnderecoUsuarioInfo = {
		NOME_TABELA : 'EnderecoUsuario',
}

function DAOEnderecoUsuario(){
 
 	this.createTable = function(tx) {    
		var sqlCreateTableEnderecoUsuario = "" +
				"	CREATE TABLE IF NOT EXISTS " +  daoEnderecoUsuarioInfo.NOME_TABELA + " (" +
				"		id INTEGER PRIMARY KEY AUTOINCREMENT," +
				"		usuario INTEGER, " +
				"		cep TEXT, " +
				"		estado TEXT, " +
				"		municipio TEXT, " +
				"		bairro TEXT, " +
				"		logradouro TEXT, " +
				"		numero TEXT, " +
				"		complemento TEXT " +
				"	)";
		tx.executeSql(sqlCreateTableEnderecoUsuario);
	}
 	
 	this.dropTable = function() {
 		var sql = 'DROP TABLE ' + daoEnderecoUsuarioInfo.NOME_TABELA;
 	    db.database.transaction(
 	        function (transaction) {
 	            transaction.executeSql(sql, [], function() {
 	            	console.log("Drop table " + daoEnderecoUsuarioInfo.NOME_TABELA);
 	            }, this.handleErrors);
 	        }
 	    );
 	}
 	
	this.salvar = function(enderecoUsuario) {
		db.database.transaction(
		    function (transaction) {
		    	var sql = 'INSERT INTO ' + daoEnderecoUsuarioInfo.NOME_TABELA + ' (usuario, cep, estado, municipio, bairro, logradouro, numero, complemento) VALUES ("' +
			    	enderecoUsuario.getUsuario().getId()  + '" , "' + 
			    	enderecoUsuario.getCep()              + '" , "' + 
			    	enderecoUsuario.getEstado()           + '" , "' + 
			    	enderecoUsuario.getMunicipio()        + '" , "' + 
			    	enderecoUsuario.getBairro()           + '" , "' + 
			    	enderecoUsuario.getLogradouro()       + '" , "' + 
			    	enderecoUsuario.getNumero()           + '" , "' + 
			    	enderecoUsuario.getComplemento()      + '" )';
		    	transaction.executeSql(sql);
		    }
		);
	}
	
	
	this.buscar = function(usuario) {
		db.database.transaction(
	        function (transaction) {
	        	transaction.executeSql('SELECT * FROM ' +  daoEnderecoUsuarioInfo.NOME_TABELA  + ' WHERE usuario = ' + usuario.getId(),[],function(tx, results) {
 	    	    	var resultados = results.rows;
 	    	    	if (resultados.length > 0) {
 	    	    		for (var i = 0; i < resultados.length; i++) {
 	    	    			var resultado = resultados[i];
	 	  	    	    	var enderecoUsuario = new EnderecoUsuario();
	 	  	    	    	enderecoUsuario.setId(resultado.id);
	 	  	    	    	enderecoUsuario.setUsuario(usuario);
	 	  	    	    	enderecoUsuario.setCep(resultado.cep);
	 	  	    	    	enderecoUsuario.setEstado(resultado.estado);
	 	  	    	    	enderecoUsuario.setMunicipio(resultado.municipio);
	 	  	    	    	enderecoUsuario.setBairro(resultado.bairro);
	 	  	    	    	enderecoUsuario.setNumero(resultado.numero);
	 	  	    	    	enderecoUsuario.setComplemento(resultado.complemento);
   	 	  	    	    	usuario.getEnderecos().push(enderecoUsuario);
 	    	    		}
 	    	    	}
    	    	
 	     	    }, this.handleErrors);
	        }
		);
	}
 	
}