var usuarioInfo = {
		memoryRows : null,
 		NOME_TABELA : 'Usuario'
}

function DAOUsuario() {
	
 	this.createTable = function(tx) {    
		var sqlCreateTableServico = "" +
				"	CREATE TABLE IF NOT EXISTS " +  usuarioInfo.NOME_TABELA + " (" +
				"		id INTEGER PRIMARY KEY AUTOINCREMENT," +
  				"		nome TEXT, " +
				"		email TEXT, " +
				"		senha TEXT, " +
				"		cep TEXT, " +
				"		estado TEXT, " +
				"		municipio TEXT, " +
				"		bairro TEXT, " +
				"		logradouro TEXT, " +
				"		numero TEXT, " +
				"		complemento TEXT " +
  				"	)";
		tx.executeSql(sqlCreateTableServico);
	}
 	
	this.salvar = function(usuario) {
		db.database.transaction(
		    function (transaction) {
		    	var sql = 'INSERT INTO ' + usuarioInfo.NOME_TABELA + ' (nome, email, senha, cep, estado, municipio, bairro, logradouro, numero, complemento) VALUES ("' +
 		    		usuario.getNome()       + '" , "' + 
		    		usuario.getEmail()      + '" , "' + 
		    		usuario.getSenha()      + '" , "' + 
		    		usuario.getCep()        + '" , "' + 
		    		usuario.getEstado()     + '" , "' + 
		    		usuario.getMunicipio()  + '" , "' + 
		    		usuario.getBairro()     + '" , "' + 
		    		usuario.getLogradouro() + '" , "' + 
		    		usuario.getNumero()     + '" , "' + 
  		    		usuario.getComplemento()+ '" )';
		    	transaction.executeSql(sql);
		    	
		    	transaction.executeSql('SELECT MAX(ID) AS maxId  FROM ' + usuarioInfo.NOME_TABELA + ' order by id ',[],function(tx, results) {
 		 	    	usuario.setId(results.rows[0].maxId);
		  	    });
		    }
		);
	}
	
	this.update = function(usuario) {
		db.database.transaction(
		    function (transaction) {
		    	var sql = 'UPDATE ' + usuarioInfo.NOME_TABELA
		    	+ ' SET nome = "' + usuario.getNome() 
		    	+ '" , email = "' + usuario.getEmail() 
		    	+ '" , senha = "' + usuario.getSenha() 
		    	+ '" , cep = "' + usuario.getCep() 
		    	+ '" , estado = "' + usuario.getEstado() 
		    	+ '" , municipio =  "' + usuario.getMunicipio() 
		    	+ '" , bairro = "' + usuario.getBairro() 
		    	+ '" , logradouro = "' + usuario.getLogradouro() 
		    	+ '" , numero = "' + usuario.getNumero() 
		    	+ '" , complemento = "' + usuario.getComplemento() 
		    	+ '"   WHERE id = ' + usuario.getId();
		    	transaction.executeSql(sql);
  		    }
	    );
	}
	
	this.buscar = function(usuario) {
		db.database.transaction(
	        function (transaction) {
	        	transaction.executeSql('SELECT * FROM ' +  usuarioInfo.NOME_TABELA  + ' WHERE id = ' + usuario.getId(),[],function(tx, results) {
 	    	    	if (results.rows.length > 0) {
 	 	    	    	var resultado = results.rows[0];
 	    	    		usuario.setId(resultado.id);
 	    	    		usuario.setNome(resultado.nome);
 	    	    		usuario.setEmail(resultado.email);
 	    	    		usuario.setSenha(resultado.senha);
 	    	    		usuario.setCep(resultado.cep);
 	    	    		usuario.setEstado(resultado.estado);
 	    	    		usuario.setMunicipio(resultado.municipio);
 	    	    		usuario.setBairro(resultado.bairro);
 	    	    		usuario.setLogradouro(resultado.logradouro);
 	    	    		usuario.setNumero(resultado.numero);
 	    	    		usuario.setComplemento(resultado.complemento);
 	    	    	}
  	     	    } ,this.handleErrors);
	        }
		);
	}
	
	this.getMemoryRows = function() {
		return usuarioInfo.memoryRows;
	}
	
	this.deletar = function(usuario) {
		db.database.transaction(
	        function (transaction) {
	            var sql = 'DELETE FROM ' +  usuarioInfo.NOME_TABELA + ' WHERE id = ' + usuario.getId();
		    	transaction.executeSql(sql);
		    	usuario.setId(null);
 	        }
	    );
 	}
 	
 	this.dropTable = function() {
 		var sql = 'DROP TABLE ' + usuarioInfo.NOME_TABELA;
 	    db.database.transaction(
 	        function (transaction) {
 	            transaction.executeSql(sql, [], function() {
 	            	console.log("Drop table " + usuarioInfo.NOME_TABELA);
 	            }, this.handleErrors);
 	        }
 	    );
 	}
 	
	this.listarAll = function() {
	    db.database.transaction(this.query, this.handleErrors);    
	}
	
	this.query = function(tx) {
	    tx.executeSql('SELECT * FROM ' +  usuarioInfo.NOME_TABELA  + ' WHERE nome is not null order by nome ',[],function(tx, results) {
	    	usuarioInfo.memoryRows = results.rows;    
 	    }, this.handleErrors);
	}
 	
	this.sucesso = function(tx, results) {
		console.log("sucesso ao executar o sql.");
	}
	
	this.handleErrors = function(err) {
	    console.log("Error processing SQL: " + err.code);
	}
}