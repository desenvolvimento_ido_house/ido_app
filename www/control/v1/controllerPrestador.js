
function ControllerPrestador() {
	
	var businessPrestador = new BusinessDelegatePrestador();
	var businessEnderecoAtendimentoPrestador = new BusinessDelegateEnderecoAtendimentoPrestador();
	var businessDelegateEnderecoAtendimentoBairroPrestador = new BusinessDelegateEnderecoAtendimentoBairroPrestador();
	var businessDelegateServico = new BusinessDelegateServico();
   	var controllerUsuario = new ControllerUsuario();
	
	this.configurar = function() {
		
		//configurar controller
 		controllerUsuario.configurar();
 		
		//iniciar business
 		businessPrestador.iniciar();
 		businessEnderecoAtendimentoPrestador.iniciar();
 		businessDelegateEnderecoAtendimentoBairroPrestador.iniciar();
 		businessDelegateServico.iniciar();
 		
		mudarCPFCNPJ();
		$(document).on('keyup', '#inputCnpjCpf', validateKeyCpfCnpj);
		$(document).on('keyup', '#inputCnpjCpf', onKeyUpCpf);
		$(document).on('blur', '#inputCnpjCpf', onBlurCpf);
  		
 		//comandos dos botoes
   		$(document).on('vclick','#btSalvarNovoPrestador', this.salvar ); 
   		
	}

	var salvouPrestador = false;
	
	this.salvar = function() {  
		var cnpjCpf = getCnpjCpf();
		var dataNascimento = getDataNascimento();
 		var tipoPessoa = getTipoPessoa();
  		
		if (businessPrestador.getPrestador().getId() != null) {
	   		
	   		businessEnderecoAtendimentoPrestador.getEnderecoAtendimentoPrestador().setPrestador(businessPrestador.getPrestador());
	   		businessEnderecoAtendimentoPrestador.excluirAll();
	   		
	   		businessDelegateEnderecoAtendimentoBairroPrestador.getEnderecoAtendimentoBairroPrestador().setPrestador(businessPrestador.getPrestador());
	   		businessDelegateEnderecoAtendimentoBairroPrestador.excluirAll();
	   		
	   		businessPrestador.getBusinessServicoPrestador().getServicoPrestador().setPrestador(businessPrestador.getPrestador());
	   		businessPrestador.getBusinessServicoPrestador().excluirAll();
		}
  		
		//salvar prestador
 		var objPrestador = businessPrestador.getPrestador();
 		objPrestador.setCnpjCpf(cnpjCpf)
		objPrestador.setTipoPessoa(beanPrestadorInfo.PF);
 		var objUsuario = objPrestador.getUsuario();
 		objUsuario.setId(sessionStorage.getItem("idUsuario"));
		controllerUsuario.getBusinessDelegateUsuario().getDaoUsuario().buscar ( objUsuario );
    		
 		//TODO VERIFICAR ESSE SAVE
 		if (tipoPessoa == 'pf') {
 			objPrestador.setTipoPessoa(beanPrestadorInfo.PF);
 		} else {
 			objPrestador.setTipoPessoa(beanPrestadorInfo.PJ); 			
 		}
 		objPrestador.setDataNascimento(dataNascimento);
 		objPrestador.setUsuario(objUsuario);
				
 		if($('#servicoPrincipal').val() == null || $('#servicoPrincipal').val() == ''){
 			alert("É necessário preencher ao menos um serviço.");
 		}
 		
 		retorno = businessPrestador.salvar();
		
		if (!retorno) {
			return;
		}
  		
 		$('#divServicos input').each(function(){
 			var servico = $(this).val();
  			if (servico != '' ) {
  			 	var businessDelegateServicoPrestador = new BusinessDelegateServicoPrestador();
  			 	businessDelegateServicoPrestador.getServicoPrestador().setDescricao(servico)
  			 	businessDelegateServicoPrestador.getServicoPrestador().setPrestador(objPrestador);
  			 	businessDelegateServicoPrestador.salvar();
  			 	
  			 	var businessDelegateServicoSalvar = new BusinessDelegateServico();
  			 	businessDelegateServicoSalvar.getServico().setId(null);
  			 	businessDelegateServicoSalvar.getServico().setServico(servico);
  			 	businessDelegateServicoSalvar.getServico().setDetalhe('servico cadastrado pelo prestador ' + objUsuario.getNome());
  			 	businessDelegateServicoSalvar.salvar();
  			 }
 		});
 	     		

  		nomebairrosAtendimento.push('enderecos');
  		$.each(nomebairrosAtendimento, function(i, row) {
 			
 			businessEnderecoAtendimentoPrestador = new BusinessDelegateEnderecoAtendimentoPrestador();
 			businessEnderecoAtendimentoPrestador.getEnderecoAtendimentoPrestador().setPrestador(businessPrestador.getPrestador());
	 		var bairros = [];
	 		
	  		$('#' + row + ' input').each(function() {
	  			
	   			var input = $(this);
	  			var tagName = input.attr('name');
	  			var valor = input.val();
	  			
	  			if ( (tagName.indexOf("cepAtendimento") > -1)) {
 	  				businessEnderecoAtendimentoPrestador.getEnderecoAtendimentoPrestador().setCep(valor);
	  			}
	  			
	  			if ( (tagName.indexOf("estadoAtendimento") > -1)) {
	  				businessEnderecoAtendimentoPrestador.getEnderecoAtendimentoPrestador().setEstado(valor);
	  			}

	  			if ( (tagName.indexOf("municipioAtendimento") > -1)) {
	  				businessEnderecoAtendimentoPrestador.getEnderecoAtendimentoPrestador().setMunicipio(valor);
	  			}
	  			
	  			if (tagName.indexOf("bairro") > -1) {
	   				bairros.push(valor); 	
	  			}
	  			
	 		});
	  		
	  		retorno = businessEnderecoAtendimentoPrestador.salvar();
  		    $.each(bairros, function(i, row) {
		    	var businessAtendimentoBairroPrestador = new BusinessDelegateEnderecoAtendimentoBairroPrestador();
		    	businessAtendimentoBairroPrestador.getEnderecoAtendimentoBairroPrestador().setPrestador(businessPrestador.getPrestador());
	 	    	businessAtendimentoBairroPrestador.getEnderecoAtendimentoBairroPrestador().setEnderecoAtendimentoPrestador(businessEnderecoAtendimentoPrestador.getEnderecoAtendimentoPrestador());
	 	    	businessAtendimentoBairroPrestador.getEnderecoAtendimentoBairroPrestador().setBairro(row);
	 	    	businessAtendimentoBairroPrestador.salvar();
		    });  		
 		});
  		if(retorno){
  			salvouPrestador = true;
  		}
  		entrar();
 	}

	function entrar(){
		if(salvouPrestador){
			$('#ancoraEntrar').click();
			salvouPrestador = false;
		}
	}

	function mudarCPFCNPJ(){
		$("input[name='radioTipoPessoa']").bind( "change", "#", function() {
	        if ($("#radioFisica").is(":checked")) {
	    		$('#cpfoucnpj').html ('CPF:');
	    		$('#liDataNascimento').css('display','');
	    		$('#inputCnpjCpf').val('');
	    		$(document).off('keyup', '#inputCnpjCpf', onKeyUpCnpj);
	    		$(document).off('blur', '#inputCnpjCpf', onBlurCnpj);
	    		$(document).on('keyup', '#inputCnpjCpf', validateKeyCpfCnpj);
	    		$(document).on('keyup', '#inputCnpjCpf', onKeyUpCpf);
	    		$(document).on('blur', '#inputCnpjCpf', onBlurCpf);
	        }
	        else if ($("#radioJuridica").is(":checked") ){
	    		$('#cpfoucnpj').html ('CNPJ:');
	    		$('#liDataNascimento').css('display','none');
	    		$('#inputCnpjCpf').val('');
	    		$(document).off('keyup', '#inputCnpjCpf', onKeyUpCpf);
	    		$(document).off('blur', '#inputCnpjCpf', onBlurCpf);
	    		$(document).on('keyup', '#inputCnpjCpf', validateKeyCpfCnpj);
	    		$(document).on('keyup', '#inputCnpjCpf', onKeyUpCnpj);
	    		$(document).on('blur', '#inputCnpjCpf', onBlurCnpj);
	        }
	    });
	}

	function getCnpjCpf() {
		 return $('#inputCnpjCpf').val();
	}

	function getDataNascimento() {
		 return $('#dataNascimento').val();
	} 
		
	function getTipoPessoa() {
		if ($("#radioFisica").is(":checked")) {
			return 'pf';
	    }
	    else if ($("#radioJuridica").is(":checked") ){
	    	return 'pj';
	    }
	}	
}