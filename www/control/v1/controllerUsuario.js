function ControllerUsuario() {

	var businessDelegateUsuario = new BusinessDelegateUsuario();
	var businessDelegateTelefoneUsuario = new BusinessDelegateTelefoneUsuario();
 	var businessDelegateEmailUsuario = new BusinessDelegateEmailUsuario();
 	var businessDelegateEnderecoUsuario = new BusinessDelegateEnderecoUsuario();

	this.getBusinessDelegateUsuario = function() {
		return businessDelegateUsuario;
	}
	
	this.getBusinessDelegateTelefoneUsuario = function() {
		return businessDelegateTelefoneUsuario;
	}
	
	this.getBusinessDelegateEmailUsuario = function() {
		return businessDelegateEmailUsuario;
	}
	
	this.getBusinessDelegateEnderecoUsuario = function() {
		return businessDelegateEnderecoUsuario;
	}
	
	this.excluir = function() {
		businessDelegateUsuario.excluir();
	}
	
	this.listarAll = function() {
		businessDelegateUsuario.listarAll();
	}
	
	this.configurar = function() {
		
		businessDelegateUsuario.iniciar();
		businessDelegateTelefoneUsuario.iniciar();
		businessDelegateEmailUsuario.iniciar();
		businessDelegateEnderecoUsuario.iniciar();
		
		$(document).on('blur', '#telefonePrincipal', this.mascaraTelefone);
		$(document).on('blur', '#email', this.mascaraEmail);
		$(document).on('blur', '#emailConfirmacao', this.mascaraEmail);
		$(document).on('blur', '#emailAdicional', this.mascaraEmail);
		$(document).on('blur', '#cepPrincipal', this.mascaraCEPPrincipal);
		$(document).on('click', '#btVoltarLogin', this.voltarPaginaLogin);
		$(document).on('click', '#btCriarNovoUsuario', this.criarNovaConta);
   		$(document).on('click', '#btnSalvarUsuario', this.salvar ); 
   		$(document).on('click', '#btnSalvarUsuario', this.proximaTela ); 
	}
	
	this.mascaraTelefone = function(){
		var telefone = $(this).val().replace("(", "").replace(")", "").replace("-", "");
		if(telefone.length < 10){
			$(this).val('');
		} else {
			var telefoneMascara = '';
			for(var i = 0; telefone.length > i; i++){
				if(i==0){
					telefoneMascara += '(';
				} else if(i==2){
					telefoneMascara += ')';
				} else if(telefone.length == i + 4){
					telefoneMascara += '-';
				}
				telefoneMascara += telefone.charAt(i);
			}
			$(this).val(telefoneMascara);
		}
	}
	
	this.mascaraEmail = function(){
		var email = $(this).val();
		if(email.length > 0){
			er = /^[a-zA-Z0-9][a-zA-Z0-9\._-]+@([a-zA-Z0-9\._-]+\.)[a-zA-Z-0-9]{2}/; 
			if( !er.exec(email) ){
				 alert("Email Inválido!");
				 $(this).focus();
			}
		}
	}
	
	this.voltarPaginaLogin = function(){
		limparPagina();
	}
	
	this.criarNovaConta = function(){
		$("#login").css("display","none");
		$("#novaConta").css("display","");
		$('#nomePagina').html('Cadastro de Usuário');
	}
	
	this.mascaraCEPPrincipal = function(){
	    var cep = $(this).val().replace("-", "");
		if(cep.length != 8){
			$(this).val('');
		} else {
			var cepMascara = '';
			for(var i = 0; cep.length > i; i++){
				if(cep.length == i + 3){
					cepMascara += '-';
				}
				cepMascara += cep.charAt(i);
			}
			$(this).val(cepMascara);
		}
	}

	var salvouUsuario = false;
	
	this.salvar = function(){
		var email = getEmail();
		var emailConfirmacao = getEmailConfirmacao();
		var nome = getNome();
		var senha = getSenha();
 		var cep = getCep();
 		var estado = getEstado();
 		var municipio = getMunicipio();
 		var bairro = getBairro();
 		var logradouro = getLogradouro();
 		var numero = getNumero();
 		var complemento = getComplemento();
		
		//salvar usuario
		var objUsuario = businessDelegateUsuario.getUsuario();
 		objUsuario.setEmail(email);
 		objUsuario.setEmailConfirmacao(emailConfirmacao);
		objUsuario.setNome(nome);
		objUsuario.setSenha(senha);
		objUsuario.setCep(cep);
		objUsuario.setEstado(estado);
		objUsuario.setMunicipio(municipio);
		objUsuario.setBairro(bairro);
		objUsuario.setLogradouro(logradouro);
		objUsuario.setNumero(numero);
		objUsuario.setComplemento(complemento);
  		var retorno = businessDelegateUsuario.salvar();
 			
  		if (!retorno ) {//TODO CUIDADO POIS AS REGRAS NÃO ESTÃO SINCRONIZADAS VALIDAÇÃO BASICA POR ENQUANTO
  			return;
  		}
			
   		$('#divEmail input').each(function(){
 			var email = $(this).val();
  			if (email != '' ) {
  				var businessDelegateEmailUsuarioSalvar = new BusinessDelegateEmailUsuario();
  				businessDelegateEmailUsuarioSalvar.getEmailUsuario();
  				businessDelegateEmailUsuarioSalvar.getEmailUsuario().setUsuario(objUsuario);
  			 	businessDelegateEmailUsuarioSalvar.getEmailUsuario().setEmail(email);
  			 	businessDelegateEmailUsuarioSalvar.salvar();
  			 }
 		});
  		
 		$('#divTelefones input').each(function(){
 			var telefone = $(this).val();
  			if (telefone != '' ) {
  				var businessDelegateTelefoneUsuarioSalvar = new BusinessDelegateTelefoneUsuario();
  				businessDelegateTelefoneUsuarioSalvar.getTelefoneUsuario().setUsuario(objUsuario);
  				businessDelegateTelefoneUsuarioSalvar.getTelefoneUsuario().setTelefone(telefone);
  				businessDelegateTelefoneUsuarioSalvar.salvar();
  			 }
 		});

		businessDelegateEnderecoUsuario = new BusinessDelegateEnderecoUsuario();
		businessDelegateEnderecoUsuario.getEnderecoUsuario().setUsuario(objUsuario)
		businessDelegateEnderecoUsuario.getEnderecoUsuario().setCep(cep);
		businessDelegateEnderecoUsuario.getEnderecoUsuario().setEstado(estado);
		businessDelegateEnderecoUsuario.getEnderecoUsuario().setMunicipio(municipio);
		businessDelegateEnderecoUsuario.getEnderecoUsuario().setBairro(bairro);
		businessDelegateEnderecoUsuario.getEnderecoUsuario().setLogradouro(logradouro);
		businessDelegateEnderecoUsuario.getEnderecoUsuario().setNumero(numero);
		businessDelegateEnderecoUsuario.getEnderecoUsuario().setComplemento(complemento);
  		retorno = businessDelegateEnderecoUsuario.salvar();
  		if (!retorno ) {//TODO CUIDADO POIS AS REGRAS NÃO ESTÃO SINCRONIZADAS VALIDAÇÃO BASICA POR ENQUANTO
  			return;
  		}
 	     		
  		$.each(nomeEnderecos, function(i, row) {
 			businessDelegateEnderecoUsuario = new BusinessDelegateEnderecoUsuario();
 			businessDelegateEnderecoUsuario.getEnderecoUsuario().setUsuario(objUsuario)
 			
	  		$('#' + row + ' input').each(function() {
	  			
	   			var input = $(this);
	  			var tagName = input.attr('name');
	  			var valor = input.val();
	  			
	  			if ( (tagName.indexOf("cep") > -1)) {
  	  				businessDelegateEnderecoUsuario.getEnderecoUsuario().setCep(valor);
	  			}
	  			
	  			if ( (tagName.indexOf("estado") > -1)) {
	  				businessDelegateEnderecoUsuario.getEnderecoUsuario().setEstado(valor);
	  			}

	  			if ( (tagName.indexOf("municipio") > -1)) {
	  				businessDelegateEnderecoUsuario.getEnderecoUsuario().setMunicipio(valor);
	  			}
	  			
	  			if ( (tagName.indexOf("bairro") > -1)) {
	  				businessDelegateEnderecoUsuario.getEnderecoUsuario().setBairro(valor);
	  			}
	  			
	  			if ( (tagName.indexOf("logradouro") > -1)) {
	  				businessDelegateEnderecoUsuario.getEnderecoUsuario().setLogradouro(valor);
	  			}
	  			
	  			if ( (tagName.indexOf("numero") > -1)) {
	  				businessDelegateEnderecoUsuario.getEnderecoUsuario().setNumero(valor);
	  			}
	  			
	  			if ( (tagName.indexOf("complemento") > -1)) {
	  				businessDelegateEnderecoUsuario.getEnderecoUsuario().setComplemento(valor);
	  			}
	  		});
	  		retorno = businessDelegateEnderecoUsuario.salvar();
  		});
  		if(retorno){
  			salvouUsuario = true;
  		}
  		window.sessionStorage.setItem("idUsuario", objUsuario.getId());
	}
	
	this.proximaTela = function(){
		if(salvouUsuario){
			limparPagina();
			$('#ancoraProximaPagina').click();
			salvouUsuario = false;
		}
	}
	
	function limparPagina(){
		$("#login").css("display","");
		$("#novaConta").css("display","none");
		$('#nomePagina').html('I Do');
		$('#nome').val('');
		$('#email').val('');
		$('#emailConfirmacao').val('');
		$('#emailAdicional').val('');
		$('#senhaCadastro').val('');
		$('#telefonePrincipal').val('');
		$('#cepPrincipal').val('');
		$('#estadoPrincipal').val('');
		$('#municipioPrincipal').val('');
		$('#bairroPrincipal').val('');
		$('#logradouroPrincipal').val('');
		$('#numeroPrincipal').val('');
		$('#complementoPrincipal').val('');
	}

	function getNome() {
		 return $('#nome').val();
	}
	
	function getEmail() {
		 return $('#email').val();
	}
	
	function getEmailConfirmacao() {
		return $('#emailConfirmacao').val();
	}
	
	function getSenha() {
		 return $('#senhaCadastro').val();
	}
	
	function getCep() {
		 return $('#cepPrincipal').val();
	}
	
	function getEstado() {
		 return $('#estadoPrincipal').val();
	}
	
	function getMunicipio() {
		 return $('#municipioPrincipal').val();
	}
	
	function getBairro() {
		 return $('#bairroPrincipal').val();
	}
	
	function getLogradouro() {
		 return $('#logradouroPrincipal').val();
	}

	function getNumero() {
		return $('#numeroPrincipal').val();
	}
	
	function getComplemento() {
		return $('#complementoPrincipal').val();
	}
}