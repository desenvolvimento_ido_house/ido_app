var elementDataTemp = "";
var mask = "";
var sd = "-";
var nc = "2";
var sa = ".";
var cn = "true";
var ncb = "7";
var NUM_DIGITOS_CPF  = 11;
var NUM_DIGITOS_CNPJ = 14;
var NUM_DGT_CNPJ_BASE = 8;
/*
    element.attachEvent( "onkeyup", onKeyUp );
	element.attachEvent( "onblur", onBlur );
	element.maxLength=18;
*/
function onKeyUpCpf(){
	//alert("onKeyUpCpfCnpj")008.976.499-40
    if ( this.value != "" ){
            if ( arguments[0].which != 9 && arguments[0].which != 16 ){
            	
            		if(this.value.length > 14){
            			this.value = this.value.substring(0 , 14);
            			// return true;
            		}
                    formatNumberCpfCnpj(this);
            }
    }
    return true;
} 

function onKeyUpCnpj(){
	//alert("onKeyUpCpfCnpj")
    if ( this.value != "" ){
            if ( arguments[0].which != 9 && arguments[0].which != 16 ){
            	
            		if(this.value.length > 18){
            			this.value = this.value.substring(0 , 18);
            			// return true;
            		}
                    formatNumberCpfCnpj(this);
            }
    }
    return true;
}

function onKeyUpCpfCnpj(){
	//alert("onKeyUpCpfCnpj")
    if ( this.value != "" ){
            if ( arguments[0].which != 9 && arguments[0].which != 16 ){
                    formatNumberCpfCnpj( this );
            }
    }
    return true;
}

function onBlurCpfCnpj(){
    if ( this.value != "" ){
            formatNumberCpfCnpj(this);
            if ( !isCpfCnpj( this.value ) ){
                    alert("CPF ou CNPJ inválido!");
                    this.value = '';
                    this.focus();
                    return false;
            }
    }
    return true;
}

function onBlurCpf(){
    if ( this.value != "" ){
            formatNumberCpfCnpj(this);
            var numero = this.value.replace(/\D/g, "");
            if ( !isCpf( numero ) ){
                    alert("CPF inválido!");
                    this.value = '';
                    this.focus();
                    return false;
            }
    }
    return true;
}

function onBlurCnpj(){
    if ( this.value != "" ){
            formatNumberCpfCnpj(this);
            var numero = this.value.replace(/\D/g, "");
            if ( !isCnpj( numero ) ){
                    alert("CNPJ inválido!");
                    this.value = '';
                    this.focus();
                    return false;
            }
    }
    return true;
}

function formatNumberCpfCnpj(obj){
    var numberOfChars = 0;
    for (var i=obj.value.length - 1; i >= 0; i-- ){
            var char = obj.value.substr(i,1);
            var charCode = char.charCodeAt(0);
            if ( validateKeyCodeCpfCnpj( charCode ) ){
                    elementDataTemp = char + "" + elementDataTemp;
                    if ( elementDataTemp.length == nc ){
                            elementDataTemp = sd + "" + elementDataTemp;
                            numberOfChars ++;
                    }
                    if ( obj.value.length <= 14  ) {
                            if ( elementDataTemp.length - nc - numberOfChars > 1 && i > 0 ){
                                    if ( ( (elementDataTemp.length - nc - numberOfChars) / 3 ).toString().indexOf(".") == -1 ){
                                        elementDataTemp = sa + "" + elementDataTemp;
                                        numberOfChars ++;
                                    }
                            }
                    }
                    else{
                            if ( elementDataTemp.length <= 18 ) {
                                    if ( elementDataTemp.length == ncb ){
                                            elementDataTemp = "/" + elementDataTemp;
                                            numberOfChars ++;
                                    }
                                    if ( elementDataTemp.length - ( ncb - 1 ) - numberOfChars > 1 && i > 1 ){
                                            if ( ( (elementDataTemp.length - ( ncb  - 1 ) - numberOfChars) / 3 ).toString().indexOf(".") == -1 ){
                                                    elementDataTemp = sa + "" + elementDataTemp;
                                                    numberOfChars ++;
                                            }
                                    }
                            }
                    }
            }
    }
    obj.value = elementDataTemp;
    elementDataTemp = "";
}

function validateKeyCodeCpfCnpj( keyCode ){
    if (( keyCode >= 48  && keyCode <= 57 ) ||
            ( keyCode >= 144 - 48 && keyCode <= 144 - 37 )){
            return true;
    }
    else{
            return false;
    }
}

function validateKeyCpfCnpj(){
    return validateKeyCodeCpfCnpj( arguments[0].which );
}


function unformatNumberCpfCnpj(pNum){
    return String(pNum).replace(/\D/g, "").replace(/^0+/, "");
}

function dvCpfCnpj(pEfetivo, pIsCnpj){
    if (pIsCnpj==null) pIsCnpj = false;
    var i, j, k, soma, dv;
    var cicloPeso = pIsCnpj? NUM_DGT_CNPJ_BASE: NUM_DIGITOS_CPF;
    var maxDigitos = pIsCnpj? NUM_DIGITOS_CNPJ: NUM_DIGITOS_CPF;
    var calculado = pEfetivo
    //calculado = calculado.substring(2, maxDigitos);
    var result = "";

    for(j = 1; j <= 2; j++){
            k = 2;
            soma = 0;
            for (i = calculado.length-1; i >= 0; i--){
                    soma += (calculado.charAt(i) - '0') * k;
                    k = (k-1) % cicloPeso + 2;
            }
            dv = 11 - soma % 11;
            if (dv > 9) dv = 0;
            calculado += dv;
            result += dv
    }

    return result;
}

function isCpf(pCpf){
    var numero = pCpf;
    var base = numero.substring(0, numero.length - 2);
    var digitos = dvCpfCnpj(base, false);
    var algUnico, i;

    if (numero != base + digitos) return false;

    algUnico = true;
    for ( i=1; i<NUM_DIGITOS_CPF; i++){
            algUnico = algUnico && (numero.charAt(i-1) == numero.charAt(i));
    }
    return (!algUnico);
}

function isCnpj(pCnpj){
    var numero = pCnpj;
    var base = numero.substring(0, NUM_DGT_CNPJ_BASE);
    var ordem = numero.substring(NUM_DGT_CNPJ_BASE, 12);
    var digitos = dvCpfCnpj(base + ordem, true);
    var algUnico;
    if (numero != base + ordem + digitos) return false;

    algUnico = numero.charAt(0) != '0';
    for (i=1; i<NUM_DGT_CNPJ_BASE; i++){
            algUnico = algUnico && (numero.charAt(i-1) == numero.charAt(i));
    }
    if (algUnico) return false;

    if (ordem == "0000") return false;
    return (base == "00000000" || parseInt(ordem, 10) <= 300 || base.substring(0, 3) != "000");
}

function isCpfCnpj(pCpfCnpj){
    var numero = pCpfCnpj.replace(/\D/g, "");
    if (numero.length > NUM_DIGITOS_CPF)
            return isCnpj(numero)
    else
            return isCpf(numero);
}