var contador = 0;

function adicionarCampo(campo, nomeCampo, valor, tipoCampo){
	
	var container = document.getElementById(campo);
	var div = document.createElement('div');
	div.className = "ui-input-text ui-body-inherit ui-corner-all ui-shadow-inset ui-input-has-clear";
	
	var input = document.createElement('input');
	input.type = 'text';
	input.name = 'input_' + nomeCampo + '_' + contador;
	input.id = 'input_' + nomeCampo + '_' + contador;
	input.value = valor;
	
	if(tipoCampo == 'telefone'){
		input.onblur = (function mascaraTelefone(){
			var telefone = $(this).val().replace("(", "").replace(")", "").replace("-", "");
			if(telefone.length < 10){
				$(this).val('');
			} else {
				var telefoneMascara = '';
				for(var i = 0; telefone.length > i; i++){
					if(i==0){
						telefoneMascara += '(';
					} else if(i==2){
						telefoneMascara += ')';
					} else if(telefone.length == i + 4){
						telefoneMascara += '-';
					}
					telefoneMascara += telefone.charAt(i);
				}
				$(this).val(telefoneMascara);
			}
		});
	} else if(tipoCampo == 'email'){
		input.onblur = ( function mascaraEmail(){
			var email = $(this).val();
			if(email.length > 0){
				er = /^[a-zA-Z0-9][a-zA-Z0-9\._-]+@([a-zA-Z0-9\._-]+\.)[a-zA-Z-0-9]{2}/; 
				if( !er.exec(email) ){
					 alert("Email Inválido!");
					 input.focus();
				}
			}
		});
	}
	
	var ancora = document.createElement('a');
	ancora.id =  'ancora_' + nomeCampo + '_' + contador;
	div.id = 'div_' + nomeCampo + '_' + contador;
	
	ancora.onclick = function removerElemento(){
		var idAncora = this.id;
		idAncora = idAncora.replace('ancora_', 'div_')
		var div = document.getElementById(idAncora);
		div.remove();
	};
		
	contador++;
	ancora.href = "#";
	ancora.tabindex = "-1";
	ancora.className = "ui-input-clear ui-btn ui-icon-minus ui-btn-icon-notext ui-corner-all";
	ancora.title = "Clear text";
	
	//append child
	div.appendChild(input);
	div.appendChild(ancora);
	container.appendChild(div);
}

var contador2 = 1;
var nomeEnderecos = [];

function adicionarEndereco() {
	
 	var content =  '';
		content += ' <div data-role="collapsible" id="divEnderecoPrincipalFormulario' + contador2 + '" data-collapsed="false"> ';
	    content += '    <h3>Endereços</h3> ';
	    content += '	<label for="cep">Cep:</label> ';
		content += '    <input type="text" name="cep" id="cep' + contador2 + '" value="" data-clear-btn="true"> ';
 	    content += '	<label for="estado">Estado:</label> ';
 		content += '    <input type="text" name="estado" id="estado" value="" data-clear-btn="true"> ';
 		content += '    <label for="cep">Município:</label> ';
 		content += '    <input type="text" name="municipio" id="municipio" value="" data-clear-btn="true"> ';
 		content += '    <label for="bairro">Bairro</label> ';
 		content += '   <input type="text" name="bairro" id="bairro" value="" data-clear-btn="true"> ';
		content += '   <label for="logradouro">Logradouro</label> ';
		content += '   <input type="text" name="logradouro" id="logradouro" value="" data-clear-btn="true"> ';
		content += '   <label for="numero">Nº</label> ';
		content += '   <input type="text" name="numero" id="numero" value="" data-clear-btn="true"> ';
		content += '   <label for="complemento">Complemento</label> ';
		content += '   <input type="text" name="complemento" id="complemento" value="" data-clear-btn="true"> ';
	    content += ' </div> ';
	
    $("#divEnderecoPrincipal").append( content ).collapsibleset('refresh');
    $('#divEnderecoPrincipal').collapsibleset().trigger('create');
	nomeEnderecos.push('divEnderecoPrincipalFormulario' + contador2);
    
    var inputCEP = document.getElementById('cep' + contador2);
    inputCEP.onblur=(function mascaraCEP(){
		var cep = $(this).val().replace("-", "");
		if(cep.length != 8){
			$(this).val('');
		} else {
			var cepMascara = '';
			for(var i = 0; cep.length > i; i++){
				if(cep.length == i + 3){
					cepMascara += '-';
				}
				cepMascara += cep.charAt(i);
			}
			$(this).val(cepMascara);
		}
    });

    contador2++;
}

var contador3 = 1;
var nomebairrosAtendimento = [];

function adicionarEnderecoAtendimento() {
	
 	var content =  '';
 		content += ' <div data-role="collapsible"  id="divEnderecoAtendimento' + contador3 + '" data-collapsed="false"> ';
		content += '       <h3>Atendo nos endereços abaixo</h3> ';
		content += '    	<label for="cepAtendimento' + contador3 + '">Cep:</label> ';
		content += '	    <input type="text" name="cepAtendimento' + contador3 + '" id="cepAtendimento' + contador3 + '" value="" data-clear-btn="true"> ';
		content += '    	<label for="estadoAtendimento' + contador3 + '">Estado:</label> ';
		content += '	    <input type="text" name="estadoAtendimento' + contador3 + '" id="estadoAtendimento' + contador3 + '" value="" data-clear-btn="true"> ';
		content += '	    <label for="municipioAtendimento' + contador3 + '">Município:</label> ';
		content += '	    <input type="text" name="municipioAtendimento' + contador3 + '" id="municipioAtendimento' + contador3 + '" value="" data-clear-btn="true"> ';
		content += '	    <label for="bairroAtendimento' + contador3 + '">Bairros atendidos:</label>  ';
		content += '	    <div id="divBairrosAtendidos'+contador3+'" class="tirarBordas ui-input-text ui-corner-all ui-shadow-inset ui-input-has-clear"> ';
		content += '	    	<input type="text" name="bairroAtendimento'+contador3+'" id="bairroAtendimento'+contador3+'" value="" data-clear-btn="true"> ';
		content += ' 				<a href="#" tabindex="-1" aria-hidden="true" onclick="adicionarCampo(\'divBairrosAtendidos'+contador3+'\',\'bairrosAtendidos\',\'\', \'\')" class="ui-input-clear ui-btn ui-icon-plus ui-btn-icon-notext ui-corner-all" title="Clear text">Clear text</a> ';		          
		content += '        </div> ';
		content += '    </div>	';
	
    $("#divBairrosAtendimento").append( content ).collapsibleset('refresh');
    $('#divBairrosAtendimento').collapsibleset().trigger('create');

    var inputCEPAtendimento = document.getElementById('cepAtendimento' + contador3);
    inputCEPAtendimento.onblur=(function mascaraCEPAtendimento(){
		var cepAtendimento = $(this).val().replace("-", "");
		if(cepAtendimento.length != 8){
			$(this).val('');
		} else {
			var cepAtendimentoMascara = '';
			for(var i = 0; cepAtendimento.length > i; i++){
				if(cepAtendimento.length == i + 3){
					cepAtendimentoMascara += '-';
				}
				cepAtendimentoMascara += cepAtendimento.charAt(i);
			}
			$(this).val(cepAtendimentoMascara);
		}
    });
    
    nomebairrosAtendimento.push('divEnderecoAtendimento' + contador3);
    contador3++;
}

function onBlurCEPEnderecoPrincipal(inputCEPEnderecoPrincipal){
    var cep = inputCEPEnderecoPrincipal.value.replace("-", "");
	if(cep.length != 8){
		inputCEPEnderecoPrincipal.value = '';
	} else {
		var cepMascara = '';
		for(var i = 0; cep.length > i; i++){
			if(cep.length == i + 3){
				cepMascara += '-';
			}
			cepMascara += cep.charAt(i);
		}
		inputCEPEnderecoPrincipal.value = cepMascara;
	}
}

//trim completo
function trim(str) {
	return str.replace(/^\s+|\s+$/g,"");
}


function split( val ) {
	return val.split( /,\s*/ );
}

function extractLast( term ) {
	return split( term ).pop();
}

function getLatitude() {
	return latitude;
}

function getLongitude(){
	return longitude;
}

