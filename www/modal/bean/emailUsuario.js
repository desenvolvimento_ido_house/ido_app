function EmailUsuario() {
	
	var id = null;
	var usuario = new Usuario();
	var email = '';
	
	this.setId = function(_id) {
		this.id = _id;
	}
	
	this.getId = function() {
		return this.id;
	}
	
	this.setUsuario = function(_usuario) {
		this.usuario = _usuario;
	}
	
	this.getUsuario = function() {
		return this.usuario;
	}
	
	this.setEmail = function(_email) {
		this.email = _email;
	}
	
	this.getEmail = function() {
		return this.email;
	}
	
}