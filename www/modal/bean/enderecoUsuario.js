function EnderecoUsuario() {

	var id;
  	var usuario = new Usuario();

  	//dados do endereco
	var cep;
	var estado;
	var municipio;
	var bairro;
	var logradouro;
	var numero;
	var complemento;
	
 	//dados sps
 	var latitude;
 	var longitude;
 	var altitude;
 	var accuracy;
 	var altitudeAccuracy;
 	var heading;
 	var speed;
 	var enderecoValidado;
 	var latLongValidado;
	
	this.setId = function(_id) {
		this.id = _id;
	}
	
	this.getId = function() {
		return this.id;
	}
	
	this.setUsuario = function(_usuario) {
		this.usuario = _usuario;
	}
	
	this.getUsuario = function() {
		return this.usuario;
	}
	
	this.setCep = function(_cep) {
		this.cep = _cep;
	}
	
	this.getCep = function() {
		return this.cep;
	}
	
	this.setEstado = function(_estado) {
		this.estado = _estado;
	}
	
	this.getEstado = function() {
		return this.estado;
	}
	
	this.setMunicipio = function(_municipio) {
		this.municipio = _municipio;
	}
	
	this.getMunicipio = function() {
		return this.municipio;
	}
	
	this.setBairro = function(_bairro) {
		this.bairro = _bairro;
	}
	
	this.getBairro = function() {
		return this.bairro;
	}
	
	this.setLogradouro = function(_logradouro) {
		this.logradouro = _logradouro;
	}
	
	this.getLogradouro = function() {
		return this.logradouro;
	}
	
	this.setNumero = function(_numero) {
		this.numero = _numero;
	}
	
	this.getNumero = function() {
		return this.numero;
	}
	
	this.setComplemento = function(_complemento) {
		this.complemento = _complemento;
	}
	
	this.getComplemento = function() {
		return this.complemento;
	}
	
}
