function BusinessDelegateEnderecoAtendimentoBairroPrestador() {
	
	var enderecoAtendimentoBairroPrestador = new EnderecoAtendimentoBairroPrestador();
	var daoEnderecoAtendimentoBairroPrestador = new DAOEnderecoAtendimentoBairroPrestador();
	
	
	this.getEnderecoAtendimentoBairroPrestador = function() {
		return enderecoAtendimentoBairroPrestador;
	}

 	this.iniciar = function() {
 		
 		if (AppInfo.dropAllTableAoIniciar) {
 			daoEnderecoAtendimentoBairroPrestador.dropTable();
 		}
 		
	    db.database.transaction(this.criarTabela, function(err) {
	    	console.log("Error processing SQL: " + err.code);
	    }, function() {
	    	console.log("success create table " + enderecoAtendimentoBairroPrestadorInfo.NOME_TABELA);
	    });	
	}
 	
	
	this.criarTabela = function(tx) {
		daoEnderecoAtendimentoBairroPrestador.createTable(tx);
	}
	
	this.salvar = function() {
		daoEnderecoAtendimentoBairroPrestador.salvar(enderecoAtendimentoBairroPrestador);
	}
	
	this.excluirAll = function() {
		daoEnderecoAtendimentoBairroPrestador.deletarAll(enderecoAtendimentoBairroPrestador);
	}
	
}