function BusinessDelegateEnderecoUsuario(){
	
	var enderecoUsuario = new EnderecoUsuario();
	var daoEnderecoUsuario = new DAOEnderecoUsuario();

	this.getEnderecoUsuario = function() {
		return enderecoUsuario;
	}
	
 	this.iniciar = function() {
 		
 		if (AppInfo.dropAllTableAoIniciar) {
 			daoEnderecoUsuario.dropTable();
 		}
 		
	    db.database.transaction(this.criarTabela, function(err) {
	    	console.log("Error processing SQL: " + err.code);
	    }, function() {
	    	console.log("success create table " + daoEnderecoUsuarioInfo.NOME_TABELA);
	    });	
	}
 	
 	this.criarTabela = function(tx) {
 		daoEnderecoUsuario.createTable(tx)
 	}
 	
 	this.salvar = function() {

		if (enderecoUsuario.getCep() == null || enderecoUsuario.getCep() == '') {
			alert("CEP do endereço é de preenchimento obrigatório");
			return false;
		}
		
		if (enderecoUsuario.getEstado() == null || enderecoUsuario.getEstado() == '') {
			alert("Estado do endereço é de preenchimento obrigatório");
			return false;
		}
		
		if (enderecoUsuario.getMunicipio() == null || enderecoUsuario.getMunicipio() == '') {
			alert("Município do endereço é de preenchimento obrigatório");
			return false;
		}
		
		if (enderecoUsuario.getBairro() == null || enderecoUsuario.getBairro() == '') {
			alert("Bairro do endereço é de preenchimento obrigatório");
			return false;
		}
		
		if (enderecoUsuario.getLogradouro() == null || enderecoUsuario.getLogradouro() == '') {
			alert("Logradouro do endereço é de preenchimento obrigatório");
			return false;
		}
		
		if (enderecoUsuario.getNumero() == null || enderecoUsuario.getNumero() == '') {
			alert("Número do endereço é de preenchimento obrigatório");
			return false;
		}
		
 		daoEnderecoUsuario.salvar(enderecoUsuario);
 		return true;
 	}
}