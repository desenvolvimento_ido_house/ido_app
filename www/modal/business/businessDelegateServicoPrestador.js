function BusinessDelegateServicoPrestador() {
	
	var servicoPrestador = new ServicoPrestador();
	var daoServicoPrestador = new DAOServicoPrestador();
	
	this.getServicoPrestador = function() {
		return servicoPrestador;
	}
	
	this.iniciar = function() {
		
 		if (AppInfo.dropAllTableAoIniciar) {
 			daoServicoPrestador.dropTable();
 		}
 		
	    db.database.transaction(this.criarTabela, function(err) {
	    	console.log("Error processing SQL: " + err.code);
	    }, function() {
	    	console.log("success create table " + servicoPrestadorInfo.NOME_TABELA);
	    });	
	}
	
	this.criarTabela = function(tx) {
		daoServicoPrestador.createTable(tx);
	}
	
	this.salvar = function() {
		daoServicoPrestador.salvar(servicoPrestador);
	}
	
	this.excluirAll = function(){
		daoServicoPrestador.deletarAll(servicoPrestador);
	}
}