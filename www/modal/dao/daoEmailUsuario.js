var emailUsuarioInfo = {
		memoryRows : null,
		NOME_TABELA : 'EmailUsuario'
}

function DAOEmailUsuario() {
	
 	this.createTable = function(tx) {    
		var sqlCreateTable = 
			" CREATE TABLE IF NOT EXISTS " +  emailUsuarioInfo.NOME_TABELA +  " (" +
			"		id INTEGER PRIMARY KEY AUTOINCREMENT, " +
			"		usuario INTEGER, " +
			"		email TEXT , " +
			"	)";
		tx.executeSql(sqlCreateTable);
	}

 	this.dropTable = function() {
 		var sql = 'DROP TABLE ' + emailUsuarioInfo.NOME_TABELA;
 	    db.database.transaction(
 	        function (transaction) {
 	            transaction.executeSql(sql, [], function() {
 	            	console.log("drop table " + emailUsuarioInfo.NOME_TABELA);
 	            }, this.handleErrors);
 	        }
 	    );
 	}
 	
	this.buscar = function(usuario) {
		db.database.transaction(
	        function (transaction) {
	        	transaction.executeSql('SELECT * FROM ' +  emailUsuarioInfo.NOME_TABELA  + ' WHERE usuario = ' + usuario.getId(),[],function(tx, results) {
 	    	    	var resultados = results.rows;
 	    	    	if (resultados.length > 0) {
 	    	    		for (var i = 0; i < resultados.length; i++) {
 	    	    			var resultado = resultados[i];
	 	  	    	    	var emailUsuario = new EmailUsuario();
	 	  	    	    	emailUsuario.setId(resultado.id);
	 	 	    	    	emailUsuario.setEmail(resultado.email);
	 	  	    	    	usuario.getEmails().push(emailUsuario); 	
 	    	    		}
 	    	    	}
 	     	    }, this.handleErrors);
	        }
		);
	}
 	
 	this.salvar = function(emailUsuario) {
		db.database.transaction(
		    function (transaction) {
		    	var sql = 'INSERT INTO ' + emailUsuarioInfo.NOME_TABELA + ' (usuario, email) VALUES (" ' +
		    		emailUsuario.getUsuario().getId() + '" , "' +
		    		emailUsuario.getEmail() + '" , "' + 
		    	transaction.executeSql(sql);
		    	
		    }
		);
	}
 
	this.deletar = function(emailUsuario) {
		db.database.transaction(
	        function (transaction) {
	            var sql = 'DELETE FROM ' +  emailUsuarioInfo.NOME_TABELA + ' WHERE id = ' + emailUsuario.getId();
		    	transaction.executeSql(sql);
		    	emailUsuario.setId(null);
 	        }
	    );
 	}

	this.deletarAll = function(emailUsuario) {
		db.database.transaction(
	        function (transaction) {
	            var sql = 'DELETE FROM ' +  emailUsuarioInfo.NOME_TABELA + ' WHERE usuario  = ' + emailUsuario.getUsuario().getId();
		    	transaction.executeSql(sql);
  	        }
	    );
 	}
	
	this.handleErrors = function(err) {
	    console.log("Error processing SQL: " + err.code);
	}
}