var enderecoAtendimentoBairroPrestadorInfo = {
		NOME_TABELA : 'EnderecoAtendimentoBairroPrestador'
}

function DAOEnderecoAtendimentoBairroPrestador() {
	
 	this.createTable = function(tx) {    
		var sqlCreateTable = "" +
				"	CREATE TABLE IF NOT EXISTS " +  enderecoAtendimentoBairroPrestadorInfo.NOME_TABELA + " (" +
				"		id INTEGER PRIMARY KEY AUTOINCREMENT," +
				"		prestador INTEGER, " +
				"		enderecoAtendimentoPrestador INTEGER, " +
				"		bairro TEXT " +
				"	)";
		tx.executeSql(sqlCreateTable);
	}
 	
	this.dropTable = function() {
 		var sql = 'DROP TABLE ' + enderecoAtendimentoBairroPrestadorInfo.NOME_TABELA;
 	    db.database.transaction(
 	        function (transaction) {
 	            transaction.executeSql(sql, [], function() {
 	            	console.log("Drop table " + enderecoAtendimentoBairroPrestadorInfo.NOME_TABELA);
 	            }, this.handleErrors);
 	        }
 	    );
 	}
	
	this.salvar = function(enderecoAtendimentoBairroPrestador) {
		db.database.transaction(
		    function (transaction) {
		    	var sql = 'INSERT INTO ' + enderecoAtendimentoBairroPrestadorInfo.NOME_TABELA + ' (enderecoAtendimentoPrestador, prestador,  bairro ) VALUES ("' +
		    		enderecoAtendimentoBairroPrestador.getEnderecoAtendimentoPrestador().getId() + '" , "' + 
		    		enderecoAtendimentoBairroPrestador.getPrestador().getId()                    + '" , "' + 
		    		enderecoAtendimentoBairroPrestador.getBairro()                               + ' " )';
		    	transaction.executeSql(sql);
		    }
		);
	}
	
	this.buscar = function(prestador) {
		db.database.transaction(
	        function (transaction) {
	        	transaction.executeSql('SELECT * FROM ' +  enderecoAtendimentoBairroPrestadorInfo.NOME_TABELA  + ' WHERE prestador = ' + prestador.getId(),[],function(tx, results) {
 	    	    	var resultados = results.rows;
 	    	    	if (resultados.length > 0) {
 	    	    		for (var i = 0; i < resultados.length; i++) {
 	    	    			var resultado = resultados[i];
	 	  	    	    	var d = new EnderecoAtendimentoPrestador();
	 	  	    	    	enderecoAtendimentoPrestador.setId(resultado.id);
      	 	  	    	    prestador.getEnderecosAtendimento().push(enderecoAtendimentoPrestador); 	
 	    	    		}
 	    	    	}
    	    	
 	     	    }, this.handleErrors);
	        }
		);
	}
	
	this.deletarAll = function(enderecoAtendimentoBairroPrestador) {
		db.database.transaction(
	        function (transaction) {
	            var sql = 'DELETE FROM ' +  enderecoAtendimentoBairroPrestadorInfo.NOME_TABELA + ' WHERE prestador  = ' + enderecoAtendimentoBairroPrestador.getPrestador().getId();
		    	transaction.executeSql(sql);
  	        }
	    );
 	}
}