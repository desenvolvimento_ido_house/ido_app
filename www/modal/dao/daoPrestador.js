var prestadorInfo = {
		memoryRows : null,
		NOME_TABELA : 'Prestador',
}

function DAOPrestador() {
	
	this.createTable = function(tx) {    
		var sqlCreateTableServico = "" +
				"	CREATE TABLE IF NOT EXISTS " +  prestadorInfo.NOME_TABELA + " (" +
				"		id INTEGER PRIMARY KEY AUTOINCREMENT," +
				"       usuario INTEGER, " +
				"		tipoPessoa TEXT, " +
 				"		cnpjCpf TEXT, " +
				"		dataNascimento TEXT " +
				"	)";
		tx.executeSql(sqlCreateTableServico);
	}
 	
	this.salvar = function(prestador) {
		db.database.transaction(
		    function (transaction) {
		    	var sql = 'INSERT INTO ' + prestadorInfo.NOME_TABELA + ' (tipoPessoa,cnpjCpf, usuario, dataNascimento ) VALUES ("' +
 		    		prestador.getTipoPessoa()      + '" , "' + 
 		    		prestador.getCnpjCpf()         + '" , "' + 
		    		prestador.getUsuario().getId() + '" , "' + 
		    		prestador.getDataNascimento()  + ' " )';
		    	transaction.executeSql(sql);
		    	
		    	transaction.executeSql('SELECT MAX(ID) AS maxId  FROM ' + prestadorInfo.NOME_TABELA + ' order by id ',[],function(tx, results) {
		    		prestador.setId(results.rows[0].maxId);
		  	    });
		    }
		);
	}
	
	this.update = function(prestador) {
		db.database.transaction(
		    function (transaction) {
		    	var sql = 'UPDATE ' + prestadorInfo.NOME_TABELA + 
		    	' SET tipoPessoa = "' + prestador.getTipoPessoa() + 
 		    	'" , cnpjCpf = "' + prestador.getCnpjCpf() +
		    	'" , usuario = "' + prestador.getUsuario().getId() +
		    	'" , dataNascimento = "' + prestador.getDataNascimento() +
		    	'" WHERE id = ' + prestador.getId();
		    	transaction.executeSql(sql);
  		    }
	    );
	}
	
 	this.dropTable = function() {
 		var sql = 'DROP TABLE ' + prestadorInfo.NOME_TABELA;
 	    db.database.transaction(
 	        function (transaction) {
 	            transaction.executeSql(sql, [], function() {
 	            	console.log("Drop table " + prestadorInfo.NOME_TABELA);
 	            }, this.handleErrors);
 	        }
 	    );
 	}
 	
	this.deletar = function(prestador) {
		db.database.transaction(
	        function (transaction) {
	            var sql = 'DELETE FROM ' +  prestadorInfo.NOME_TABELA + ' WHERE id = ' + prestador.getId();
		    	transaction.executeSql(sql);
		    	prestador.setId(null);
 	        }
	    );
 	}
 	
	this.getMemoryRows = function() {
		return prestadorInfo.memoryRows;
	}
	
	this.listarAll = function() {
	    db.database.transaction(this.query, this.handleErrors);    
	}
	
	this.buscar = function(prestador) {
		db.database.transaction(
	        function (transaction) {
	        	transaction.executeSql('SELECT * FROM ' +  prestadorInfo.NOME_TABELA  + ' WHERE usuario = ' + prestador.getUsuario().getId(),[],function(tx, results) {
 	    	    	if (results.rows.length > 0) {
 	 	    	    	var resultado = results.rows[0];
 	    	    		prestador.setId(resultado.id);
 	    	    		prestador.setTipoPessoa(resultado.geoLatitude);
  	    	    		prestador.setCnpjCpf(resultado.cnpjCpf);
 	    	    		prestador.setDataNascimento(resultado.dataNascimento);
 	    	    	}
	     	    }, this.handleErrors);
	        }
		);
	}
	
	this.query = function(tx) {
	    tx.executeSql('SELECT * FROM ' +  prestadorInfo.NOME_TABELA  + ' WHERE nome is not null order by nome ',[],function(tx, results) {
	    	prestadorInfo.memoryRows = results.rows;    
 	    }, this.handleErrors);
	}
	
	this.sucesso = function(tx, results) {
		console.log("sucesso ao executar o sql.");
	}
	
	this.handleErrors = function(err) {
	    console.log("Error processing SQL: " + err.code);
	}
}