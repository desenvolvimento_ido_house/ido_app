var servicoPrestadorInfo = {
		memoryRows : null,
		NOME_TABELA : 'ServicoPrestador',
}

function DAOServicoPrestador() {
	
	this.createTable = function(tx) {    
		var sqlCreateTableServico = "" +
				"	CREATE TABLE IF NOT EXISTS " +  servicoPrestadorInfo.NOME_TABELA + " (" +
				"		id INTEGER PRIMARY KEY AUTOINCREMENT," +
				"       prestador INTEGER, " +
				"       descricao TEXT " +
  				"	)";
		tx.executeSql(sqlCreateTableServico);
	}
	
	this.salvar = function(servicoPrestador) {
		db.database.transaction(
		    function (transaction) {
		    	var sql = 'INSERT INTO ' + servicoPrestadorInfo.NOME_TABELA + ' (prestador, descricao) VALUES ("' +
		    		servicoPrestador.getPrestador().getId() + '" , "' +
 		    		servicoPrestador.getDescricao()         + '" )';
		    	transaction.executeSql(sql);
		    	
		    }
		);
	}
	
	this.buscar = function(prestador) {
		db.database.transaction(
	        function (transaction) {
	        	transaction.executeSql('SELECT * FROM ' +  servicoPrestadorInfo.NOME_TABELA  + ' WHERE prestador = ' + prestador.getId(),[],function(tx, results) {
 	    	    	var resultados = results.rows;
 	    	    	if (resultados.length > 0) {
 	    	    		for (var i = 0; i < resultados.length; i++) {
 	    	    			var resultado = resultados[i];
	 	  	    	    	var servicoPrestador = new ServicoPrestador();
	 	  	    	    	servicoPrestador.setId(resultado.id);
	 	  	    	    	servicoPrestador.setDescricao(resultado.descricao);
	 	  	    	    	servicoPrestador.setPrestador(prestador);
	 	  	    	    	prestador.getServicos().push(servicoPrestador); 	
 	    	    		}
 	    	    	}
    	    	
 	     	    }, this.handleErrors);
	        }
		);
	}
	
 	this.dropTable = function() {
 		var sql = 'DROP TABLE ' + servicoPrestadorInfo.NOME_TABELA;
 	    db.database.transaction(
 	        function (transaction) {
 	            transaction.executeSql(sql, [], function() {
 	            	console.log("Drop table " + servicoPrestadorInfo.NOME_TABELA);
 	            }, this.handleErrors);
 	        }
 	    );
 	}
	
	this.deletarAll = function(servicoPrestador) {
		db.database.transaction(
	        function (transaction) {
	            var sql = 'DELETE FROM ' +  servicoPrestadorInfo.NOME_TABELA + ' WHERE prestador  = ' + servicoPrestador.getPrestador().getId();
		    	transaction.executeSql(sql);
  	        }
	    );
 	}
	
}